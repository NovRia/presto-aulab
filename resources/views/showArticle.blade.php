<x-layout>
  <x-navbar></x-navbar>
  <section class="container-fluid mt-5 bgShow">
    <div class="row justify-content-center mt-5">
      <div class="col-12 d-flex justify-content-center">
        <span class="titlespag display-4 px-4 h1">{{$article->category->type_it}}</span>
      </div>
      <div class="row mt-5 justify-content-center">
        <div class="col-12 col-lg-3 card-custom-detright">
          <div class="card-body mt-4 d-flex flex-column justify-content-around">
            <h5 class="card-title h3 mb-3 titles ps-3 op-100">{{$article->title}}</h5>
            <p class="card-text h6 ps-3 op-100"><i class="fa-solid fa-tag"></i> {{$article->price}} €</p>
            <p class="card-text lead my-4 ps-3 op-100"><i class="fa-regular fa-comment-dots"></i> {{$article->description}}</p>
            <p class="card-text h6 text--green ps-3 op-100">{{__('ui.date')}}: {{$article->created_at->format('d/m/y')}}</p>
            <a class="nav-link" href="{{route('authorIndex', compact('article'))}}">
              <p class="card-text h6 text--green ps-3 pb-3 op-100">{{__('ui.author')}}: {{$article->user->name ?? ''}}</p>
            </a>
          </div>
        </div>
        <div class ="col-12 col-lg-6 card-custom-det p-4">
          <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
            @if ($article->images)
            <div class="carousel-inner">
              @foreach ($article->images as $image)
              <div class="carousel-item @if($loop->first)active @endif">
                <img src="{{Storage::url($image->path)}}" class="img-fluid p-3 rounded" alt="">
              </div>
            
              @endforeach
            </div>
            @else
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="https://picsum.photos/600" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://picsum.photos/602" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://picsum.photos/601" class="d-block w-100" alt="...">
              </div>
            </div>

            @endif

            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
              <span aria-hidden="true"><i class="fa-solid fa-caret-right fa-3x"></i></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>


        </div>
      </div>
    </div>
  </section>
  <x-footer></x-footer>
</x-layout>