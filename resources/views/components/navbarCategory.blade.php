{{-- navbar responsive --}}
<div class="container-fluid d-flex justify-content-evenly">
  <nav class="navbar navbar-dark fixed-top" id="navbarCat">
    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="offcanvas offcanvas-end bg--dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
      <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Menu</h5>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
        <ul class="navbar-nav mb-2 mb-lg-0 mt-3">
          <li>
            <form action="{{route('searchArticles')}}" method="GET" class="d-flex" role="search">
              <input name="searched" class="form-control searchbar" type="search" placeholder="{{__('ui.search')}}" aria-label="Search">
              <button class="btn" type="submit"><i class="fa-solid fa-magnifying-glass text--white"></i></button>
            </form>
          </li>
          <li class="nav-item ps-1">
            <a class="nav-link mt-5 ms-1" href="/">Home</a>
          </li>
          <li class="nav-item ps-1">
            <a class="nav-link" href="{{route('indexArticle')}}">{{__('ui.ads2')}}</a>
          </li>
          <li class="nav-item dropdown ms-1">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{__('ui.category')}}
            </a>
            
            
            <ul class="dropdown-menu dropdown-menu-dark">
              @foreach ($categories as $category)
              
              <li class="nav-item">
                <a class="nav-link" href="{{route('showCategory', compact('category'))}}">{{$category->type_it}}</a>
              </li>
              @endforeach
            </ul>
          </li>
          
          @auth
          <li class="nav-item ps-1">
            <a class="nav-link" href="{{route('articleForm')}}">{{__('ui.create-ads')}}</a>
          </li>
          <li class="nav-item ps-1">
            <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a>
          </li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
          @if (Auth::user()->is_revisor)
          <li class="nav-item">
            <div class="d-flex flex-column">
              <a href="{{route('indexRevisor')}}" class="nav-link position-relative ps-1" aria-current="page"> {{__('ui.revisor2')}}
                <span class="badge rounded-pill bg--orange">{{App\Models\Article::toBeRevisionedCount()}}
                  <span class="visually-hidden">unread messages</span>
                  </span>
              </a>

            </div>
            
          </li>
          @endif
          @else
          <li class="nav-item ps-1">
            <a class="nav-link" href="{{route('login')}}">{{__('ui.login2')}}</a>
          </li>
          <li class="nav-item ps-1">
            <a class="nav-link" href="{{route('register')}}">{{__('ui.register2')}}</a>
          </li>
          @endauth
          <li class="nav-item">
            <x-_locale lang="it"/>
            <x-_locale lang="en"/>
            <x-_locale lang="fr"/>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>



{{-- navbar pagine--}}

<div class="container-fluid" id="cats">
  <div class="row justify-content-evenly text-center bgnav2">
    <div class="col-12"> 
      <ul class="d-flex justify-content-between linknav">
        <li> <a class="navbar-brand" href="/"><i class="fa-solid fa-house text--orange ms-2"></i></a></li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('indexArticle')}}">{{__('ui.ads2')}}</a>
        </li>
        <li class="nav-item dropdown ms-1">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            {{__('ui.category')}}
          </a>
          <ul class="dropdown-menu dropdown-menu-dark">
            @foreach ($categories as $category)
            
            <li class="nav-item ps-2">
              <a class="nav-link" href="{{route('showCategory', compact('category'))}}">{{$category->type_it}}</a>
            </li>
            @endforeach
          </ul>
        </li>
        <li class="nav-item">
          <x-_locale lang="it"/>
          <x-_locale lang="en"/>
          <x-_locale lang="fr"/>
        </li>
      </ul>
    </div>
  </div>
</div>