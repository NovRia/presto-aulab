<?php

use App\Models\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('type_it');
            /* $table->string('type-en');
            $table->string('type-fr'); */
            $table->timestamps();
        });

        $categories=['Abbigliamento', 'Arredamento', 'Cucina', 'Elettrodomestici', 'Film', 'Libri', 'Musica', 'Oggettistica', 'Sport', 'Vino e liquori'];
        foreach($categories as $category){
            Category::create(['type_it'=>$category]);
        }


        
       /*  $categories = [
            ['Abbigliamento', 'Clothes', 'Vêtements'],
            ['Arredamento', 'Furniture', 'Meubles' ],
            ['Cucina', 'Cooking', 'Cuisine' ],
            ['Elettrodomestici', 'Appliances', 'Appareils ménagers' ],
            ['Film', 'Movies', 'Film' ],
            ['Libri', 'Books', 'Livres' ],
            ['Musica', 'Music', 'Musique' ],
            ['Oggettistica', 'Small objects', 'Petit objets' ],
            ['Sport', 'Sport', 'Sport' ],
            ['Vino & liquori', 'Wine & spirits', 'Vins & spiritueux' ],
        ];
        
        foreach($categories as $category){
            Category::create(['type-it'=>$category[0]]);
        } */
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
