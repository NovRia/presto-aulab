<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RequestRevisor extends Model
{
    use HasFactory;


    public $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function build(){
        return $this->from('presto.it@noreply.com')->view('mail.requestRevisor');
    }
}
