<x-layout>
    <x-navbar></x-navbar>
<section class="container-fluid min-vh-100 mt-1 bgArtForm">
    <div class="row justify-content-center form-responsive">
        <div class="col-11 col-xl-6 col-lg-8 col-md-10 bgform mt-5">
            <h1 class="text-center titles pb-5 my-3">{{__('ui.article-form')}}</h1>
            <div class=""><livewire:create-form/></div>

        </div>
    </div>
</section>
<x-footer></x-footer>
</x-layout>