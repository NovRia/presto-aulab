<?php
return [
    'allArticles' => 'DERNIERS PRODUITS',
    'payoff' => 'La plus grande plateforme en ligne de produits vintage',
    'detail' => 'Détails',
    'category' => 'Catégorie',
    'date' => 'Date de publication',
    'index' => 'INDEX DES ANNONCES',
    'author' => 'Auteur',
    'no-articles' => 'Il n\'y a pas d\'annonces',
    'no-ads' => 'Il n\'y a pas d\'annonces pour cette catégorie',
    'ads' => 'Publier des annonces',
    'login' => 'IDENTIFIEZ-VOUS',
    'email' => 'Adresse e-mail',
    'psw' => 'Mot de passe',
    'no-reg' => 'Pas enregistré?',
    'no-log' => 'Pas enregistré ne?',
    'click' => 'Cliquez ici!',
    'login2' => 'Identifiez-vous',
    'register' => 'CRÉER UN COMPTE',
    'name' => 'Prénom',
    'c-psw' => 'Entrez le mot de passe à nouveau',
    'register2' => 'Créer un compte',
    'work' => 'Voulez-vous travailler avec nous?',
    'reg-click' => 'Inscrivez-vous et cliquez ici !',
    'revisor' => 'Devenir réviseur',
    'revisor2' => 'Réviseur',
    'search' => 'Cherchez',
    'create-ads' => 'Créez votre annonce',
    'ads2' => 'Annonces',
    'message1' => 'Félicitations, vous avez accepté l\'annonce',
    'message2' => 'Félicitations, vous avez refusé l\'annonce',
    'accept' => 'Accepter',
    'reject' => 'Déclin',
    'article-form' => 'Remplissez le formulaire pour insérer votre annonce',
    'name-form' => 'Écrivez le nom du produit',
    'price-form' => 'Entrez le prix du produit',
    'category-form' => 'Entrez la catégorie de produit',
    'category-choose' => 'Choisissez la catégorie',
    'description-form' => 'Écrivez la description du produit',
    'create-article'=> 'Créez votre annonce',
    'ads-rev1' => 'Voici l\'annonce à examiner',
    'ads-rev2' => 'Il n\'y a pas d\'annonces à examiner',
    'message' => 'Annonce soumise pour examen',
    'message3' => 'Compliments! Vous avez postulé avec succès pour devenir évaluateur!',
    'message4' => 'Compliments! L\'utilisateur est devenu un réviseur',
    'preview' => 'Aperçu des photos:',
    'delete' => 'Dégager',










];
