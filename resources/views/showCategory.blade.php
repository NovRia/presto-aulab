<x-layout>
  <x-navbar></x-navbar>
  <section class="container-fluid mt-5 min-vh-100 bgCategory">
    <div class="container">
      
      <div class="row justify-content-center min-vh-100">
        <div class="col-12 mt-5 mb-3">
          <div class="titlespag display-4 px-4 h2 text-center">{{$category->type_it}}</div>
        </div>
        @forelse ($category->articles as $article)
        <div class="col-12 col-xl-3 col-lg-4 col-md-6 py-3 my-4 card-responsive">
          <div class="card-welcome" onclick="this.classList.toggle('expanded')">
            <img class="img-fluid label-welcome img-card"src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(260,260) : 'https://picsum.photos/300'}}" alt="prodotto">
            <h2 class="title title-2">{{$article->title}}</h2>
            <div class="text1">
              <div class="text-content">
                <p class="">{{$article->category->type_it}}</p>
                <p class="">{{$article->price}} €</p>
                <div class="body-text"> <p class="d-flex justify-content-center"><a href="{{route('showArticle', compact('article'))}}" class="btn btnbg text-center m-2 px-5">{{__('ui.detail')}}</a></p></div>
              </div>
            </div>
            <i class="fa-solid fa-chevron-down chevron text--orange"></i>
          </div>
        </div>
        @empty
        <div class="col-12">
          <h3 class="text-center">{{__('ui.no-ads')}}</h3>
          <span class="d-flex justify-content-center"><a href="{{route('articleForm')}}" class="btn btnbg text-center m-2 h3 px-5">{{__('ui.ads')}}</a></span>
        </div>
        
        @endforelse
      </div>
    </div>
    
  </section>
  <x-footer></x-footer>
</x-layout>