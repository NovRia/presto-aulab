<div>
    @if (session('message'))
    <div class="alert alert-success">
        {{__('ui.message')}}
    </div>
    @endif
    <form wire:submit.prevent="store">
        @csrf
        <div class="my-4">
            <label class="text-form">{{__('ui.name-form')}}:</label>
            <input type="text"  wire:model='title' class="form-control @error('title') is-invalid @enderror">
            @error('title')
            {{$message}}
            
            @enderror 
        </div>
        <div class="my-4">
            <label class="text-form">{{__('ui.price-form')}}:</label>
            <input type="number" class="form-control @error('price') is-invalid @enderror" wire:model='price'>
            @error('price')
            {{$message}}
            
            @enderror 
        </div>
        <div class="my-4">
            <label class="text-form" for="category">{{__('ui.category-form')}}:</label>
            <select wire:model.defer='category' id="category" class='form-control'>
                <option value="">{{__('ui.category-choose')}}</option>
                @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->type_it}}</option>
                @endforeach
            </select>
            
        </div>
        <div class="my-4 d-flex flex-column">
            <label class="text-form">{{__('ui.description-form')}}:</label>
            <textarea name="" id="" cols="30" rows="5" wire:model='description' class="form-control @error('description') is-invalid @enderror"></textarea>
            @error('description')
            {{$message}}
            
            @enderror 
        </div>
        <div class="my-4 d-flex flex-column">
            <input wire:model="temporary_images" type="file" name="images" multiple class="text-form form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Img"/>
            @error('temporary_images.*')
            {{$message}}
            @enderror 
        </div>
        @if (!empty($images))
        <div class="row">
            <div class="col-12">
                <p>{{__('ui.preview')}}</p>
                <div class="row rounded border-metal shadow py-4 justify-content-evenly">
                    @foreach ($images as $key=>$image)
                    <div class="col-12 col-lg-4 my-4">
                        {{-- modificare img-preview --}}
                        <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}})">
                        </div>
                        <button type="button" class="btn btn-revisor shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('ui.delete')}}</button>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <p class="d-flex justify-content-center"><button type="submit" class="btn btnbg px-5 my-3 shadow text--white">{{__('ui.create-article')}}</button></p>
    </form>
    
</div>
