<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* rotta di tipo GET per la homepage */
Route::get('/',[ProductController::class, ('welcome')])->name('welcome');


/* rotta di tipo GET per la creazione della pagina destinata al form da compilare per aggiungere prodotti */
Route::get('/article-form', [ProductController::class, ('articleForm')])->middleware('auth')->name('articleForm');

/* rotta di tipo GET per la pagina di dettagglio delle singole categorie */
Route::get('/showCategory/{category}', [ProductController::class, ('showCategory')])->name('showCategory');

// rotta di tipo GET per la pagina di dettaglio dei singoli prodotti
Route::get('/showArticle/{article}', [ProductController::class, ('showArticle')])->name('showArticle');

// rotta di tipo GET per la pagina index dei prodotti
Route::get('/indexArticle', [ProductController::class, ('indexArticle')])->name('indexArticle');

// rotta di tipo GET per la pagina index dei prodotti pubblicati dall'utente
Route::get('/myIndex', [ProductController::class, ('myIndex')])->name('myIndex');

// rotta di tipo GET per la pagina index dei prodotti pubblicati da uno specifico utente
Route::get('authorIndex/{article}', [ProductController::class, ('authorIndex')])->name('authorIndex');

/* rotta di tiop GET per la sezione destinata al revisore */
Route::get('/revisorHome', [RevisorController::class, 'indexRevisor'])->middleware('isRevisor')->name('indexRevisor');

/* rotta di tipo PATCH per accettare l'annuncio da parte del revisore (patch perchè deve modificare il campo 'is_accepted' nella table 'article' ) */
Route::patch('/acceptArticle/{article}', [RevisorController::class, 'acceptArticle'])->middleware('isRevisor')->name('revisorAcceptArticle');

/* rotta di tipo PATCH per rifiutare l'annuncio da parte del revisore (patch perchè deve modificare il campo 'is_accepted' nella table 'article' ) */
Route::patch('/rejectArticle/{article}', [RevisorController::class, 'rejectArticle'])->middleware('isRevisor')->name('revisorRejectArticle');

/* Rotta di tipo GET per richiedere di diventare revisore */
Route::get('/requestRevisor', [RevisorController::class, 'requestRevisor'])->middleware('auth')->name('requestRevisor');

/* Rotta di tipo GET per rendere un utente revisore */
Route::get('/makeRevisor/{user}', [RevisorController::class, 'makeRevisor'])->name('makeRevisor');

/* rotta di tipo GET per la ricerca di articoli */
Route::get('/searchArticles', [ProductController::class, 'searchArticles'])->name('searchArticles');


/* rotta di tipo POST per cambiare lingua la sito */
Route::post('/language/{lang}', [ProductController::class, 'setLanguage'])->name('setLocale');



