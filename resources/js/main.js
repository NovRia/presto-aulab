let btncircleADD = document.querySelector('.btncircleADD')
let btncircleUP = document.querySelector('.btncircleUP')

window.addEventListener('scroll', ()=>{
    let scrolled = window.scrollY;
    if(scrolled >= 350){
        btncircleADD.classList.remove('d-none')
    }else{
        btncircleADD.classList.add('d-none')
    }
})

window.addEventListener('scroll', ()=>{
    let upscroll = window.scrollY;
    if(upscroll >=350){
        btncircleUP.classList.remove('d-none')
    }else{
        btncircleUP.classList.add('d-none');
    }
})

// function topFunction() {
//     body.scrollTop = 0;
//     btncircleUP.scrollTop = 0;
//   }

var swiper = new Swiper(".mySwiper", {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false,
    },
  });