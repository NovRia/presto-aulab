<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateForm extends Component
{
    use WithFileUploads;
    
    public $title;
    public $price;
    public $description;
    public $category;
    public $temporary_images;
    public $images = [];
    public $image;
    public $article;
    
    protected $rules =
    [
        'title'=>'required|min:4',
        'category'=>'required',
        'price'=>'required|numeric',
        'description'=>'required|min:10',
        'temporary_images.*'=>'required|image|max:1024',
        'images.*'=>'required|image|max:1024',
    ];
    
    
    
    
    
    protected $messages=[
        'required'=> 'Questo campo è richiesto',
        'temporary_images.required'=> 'L\'immagine è richiesta',
        'temporary_images.*.image'=> 'I file devono essere immagini',
        'temporary_images.*.max'=>'L\'immagine deve essere massimo di 1 MB',
        'images.image'=>'L\'immagine deve essere un\'immagine',
        'images.max'=>'L\'immagine deve massimo di 1 MB',
        
        
    ];
    
    /* public function createArticle(){
        $this->validate();
        $category=Category::find($this->category);
        $article = $category->articles()->create([
            'title'=>$this->title,
            'price'=>$this->price,
            'description'=>$this->description,
        ]);
        Auth::user()->articles()->save($article);
        session()->flash('message', 'Annuncio inviato in revisione');
        $this->cleanForm();
    } */
    
    
    public function updatedTemporaryImages(){
        if($this->validate([
            'temporary_images.*'=>'image',
            ])){
                foreach($this->temporary_images as $image){
                    $this->images[]=$image;
                }
            }
        }
        
        public function removeImage($key){
            if(in_array($key, array_keys($this->images))){
                unset($this->images[$key]);
            }
        }
        
        // non parte il validate, controllare
        
        public function store(){
            $this->validate();
            $this->article=Category::find($this->category)->articles()->create($this->validate());
            $this->article->user()->associate(Auth::user());
            $this->article->save();
            if(count($this->images)){
                foreach($this->images as $image){
                    /* $this->article->images()->create(['path'=>$image->store('images','public',)]); */
                    $newFileName = "articles/{$this->article->id}";
                    $newImage = $this->article->images()->create(['path'=>$image->store($newFileName,'public')]);
                    
                    dispatch(new ResizeImage($newImage->path, 260, 260));
                }
                
                File::deleteDirectory(storage_path('/app/livewire-tmp'));
            }
            session()->flash('message', 'Articolo inserito con successo, sarà inserito dopo la revisione');
            $this->cleanForm();
        }
        
        
        
        public function updated($propertyName){
            $this->validateOnly($propertyName);
        }
        
        public function cleanForm(){
            $this->title='';
            $this->category='';
            $this->price='';
            $this->description='';
            $this->images=[];
            $this->image='';
            $this->temporary_images=[];
            $this->form_id=rand();
            
            
        }
        
        public function render()
        {
            return view('livewire.create-form');
        }
        
        
    }
    