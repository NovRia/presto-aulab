<footer class="container-fluid mt-4 p-5 bg--dark2 text-white">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 col-md-6 text-center">
            <h3 class="text--metal">©Presto.it</h3>
            <p class="mb-0">Parco della Vittoria</p>
            <p class="mt-0">San Vittore, 20028 (MI)</p>
            <p><i class="fa-solid fa-phone-flip text--metal"> 3926024621</i></p>
           
        </div>
        <div class="col-12 col-lg-2 col-md-6 d-flex justify-content-evenly my-4 ">
            <i class="fa-brands fa-facebook fa-2x"></i>
            <i class="fa-brands fa-twitter fa-2x"></i>
            <i class="fa-brands fa-instagram fa-2x"></i>
            <i class="fa-brands fa-linkedin fa-2x"></i>
            <i class="fa-brands fa-whatsapp fa-2x"></i>
        </div>
        <div class="col-12 col-lg-3 col-md-6 text-center">
            <p>Presto.it</p>
            <p>{{__('ui.work')}}</p>
            <p>{{__('ui.reg-click')}}</p>
            <a href="{{route('requestRevisor')}}" class="btnbg my-3 px-5">{{__('ui.revisor')}}</a>
        </div>
    
    </div>
</footer>