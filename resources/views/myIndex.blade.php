<x-layout>
    <x-navbar></x-navbar>
    <section class="container-fluid mt-5 min-vh-100 bgArtIndex">
      <div class="row justify-content-evenly">
        <div class="col-12 mt-5 d-flex justify-content-center mb-3">
          <span class="titlespag px-4 h1 shadow">i miei annunci</span>
        </div>
        
        @foreach ($nums as $num)
        <form class="d-inline" action="{{route('myIndex', compact ('num'))}}" method="GET" >
    @csrf
  
  <button type="submit" class="btn text-white">
        {{$num}}'
  </button>

</form>
  @endforeach

@auth

            @forelse ($articles as $article)
            <div class="col-12 col-md-4 py-3 my-4 d-flex justify-content-center">
                {{-- <div class="card-custom">
                  <div class="d-flex flex-column" id="btndiv">
                    <img src="{{!$article->images()->get()->isEmpty() ? Storage::url($article->images()->first()->path) : 'https://picsum.photos/300'}} " class="img-fluid img-custom-card" alt="prodotto">
                    <div class="d-flex justify-content-end bg-primary"><div class="btncircle bg--dark"><i class="fa-solid fa-bell green fa-2x"></i></div></div>
                    <div class="d-flex flex-column ms-2">
                      <p class="text-white h5">{{$article->title}}</p>
                      <p class="text-white h6">{{$article->price}} €</p>
                      <p class="text-white h6">{{__('ui.category')}}: {{$article->category->type_it}}</p>
                      <p class="h6 text--orange">{{__('ui.date')}}: {{$article->created_at->format('d/m/y')}}</p>
                      <p class="card-text text--orange">{{__('ui.author')}}: {{$article->user->name ?? ''}}</p>

                      <p class="d-flex justify-content-center"><a href="{{route('showArticle', compact('article'))}}" class="btn btnbg text-center m-2 px-5">{{__('ui.detail')}}</a></p> --}}
              <div class="card-welcome" onclick="this.classList.toggle('expanded')">
               <img class="img-fluid label-welcome img-card"src="{{!$article->images()->get()->isEmpty() ? Storage::url($article->images()->first()->path) : 'https://picsum.photos/300'}}" alt="prodotto">
                <div class="text1">
                  <div class="text-content">
                    <h2 class="title title-2">{{$article->title}}</h2>
                    <div class="body-text"> <p class="d-flex justify-content-center"><a href="{{route('showArticle', compact('article'))}}" class="btn btnbg text-center m-2 px-5">{{__('ui.detail')}}</a></p></div>
                  </div>
                </div>
                <i class="fa-solid fa-chevron-down chevron text--orange"></i>
              </div>
            </div>
            @empty
              <div class="col-12">
                <p class="text-center display-5">{{__('ui.no-articles')}}</p>
              </div>
                @endforelse
                <div class="d-flex justify-content-center align-items-center">{{$articles->links()}}</div>
            </div>
            @endauth
  </section>
  <x-footer></x-footer>
</x-layout>