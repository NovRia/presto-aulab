<x-layout>
  <x-navbar></x-navbar>
  <section class="container-fluid mt-5 min-vh-100 bgArtIndex">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 mt-5 mb-3">
          <div class="titlespag display-4 px-4 h2 text-center">{{__('ui.index')}}</div>
        </div>
        @forelse ($articles as $article)
        <div class="col-12 col-xl-3 col-lg-4 col-md-6 py-3 my-4 card-responsive">
          <div class="card-welcome" onclick="this.classList.toggle('expanded')">
            <img class="img-fluid label-welcome img-card"src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(260,260) : 'https://picsum.photos/300'}}" alt="prodotto">
            <h2 class="title title-2">{{$article->title}}</h2>
            <div class="text1">
              <div class="text-content">
                <p class="">{{$article->category->type_it}}</p>
                <p class="">{{$article->price}} €</p>
                <div class="body-text"> <p class="d-flex justify-content-center"><a href="{{route('showArticle', compact('article'))}}" class="btn btnbg text-center m-2 px-5">{{__('ui.detail')}}</a></p></div>
              </div>
            </div>
            <i class="fa-solid fa-chevron-down chevron text--orange"></i>
          </div>
        </div>
        @empty
        <div class="col-12">
          <p class="text-center display-5">{{__('ui.no-articles')}}</p>
        </div>
        @endforelse
        <div class="d-flex justify-content-center align-items-center">{{$articles->links()}}</div>
      </div>
    </div>
    
  </section>
  <x-footer></x-footer>
</x-layout>