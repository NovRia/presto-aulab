<x-layout>
  
  
  <x-mainheader></x-mainheader>
  
  {{-- messaggio di accesso negato revisore --}}
  @if (session('access.denied'))
  <div class="alert alert-danger">
    {{ session('access.denied') }}
  </div>
  @endif
  
  @if (session('message3'))
  <div class="alert alert-success">
    {{__('ui.message3')}}
  </div>
  @elseif (session('message4'))
  <div class="alert alert-success">
    {{__('ui.message4')}}
  </div>
  
  @endif
  
  
  
  {{-- card --}}
  
  <section class="container-fluid bgCategory mt-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 mt-4 pb-3">
          <div class="titlespag display-4 px-4 h2 text-center">{{__('ui.allArticles')}}</div>
        </div>
        @foreach ($articles as $article)
        
        
        <div class="col-12 col-xl-3 col-lg-4 col-md-6 py-3 my-4 card-responsive">
          <div class="card-welcome" onclick="this.classList.toggle('expanded')">
            <img class="img-fluid label-welcome img-card"src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(260,260) : 'https://picsum.photos/300'}}" alt="prodotto">
            <h2 class="title title-2">{{$article->title}}</h2>
            
            <div class="text1">
              <div class="text-content">
                <p class="">{{$article->category->type_it}}</p>
                <p class="">{{$article->price}} €</p>
                <div class="body-text"> <p class="d-flex justify-content-center"><a href="{{route('showArticle', compact('article'))}}" class="btn btnbg text-center m-2 px-5">{{__('ui.detail')}}</a></p></div>
              </div>
            </div>
            <i class="fa-solid fa-chevron-down chevron text--orange"></i>
          </div>
          
          
          
          
        </div>
        @endforeach
        
      </div>
      {{-- <div class="row" id="btndiv">
        <div class="col-12 d-flex align-items-end flex-column bg-primary">
          <span class="btncircleADD d-none"><i class="fa-solid fa-circle-plus fa-2x green"></i></span>
          <span class="btncircleUP d-none"><i class="fa-solid fa-circle-arrow-up fa-2x orange"></i></span>
        </div>
      </div> --}}
    </div>
  </section>
  
  
  <x-footer></x-footer>
</x-layout>