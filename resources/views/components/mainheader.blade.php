<header class="container-fluid"> 
  <div class="row justify-content-center align-items-center">
    <nav class="navbar navbar-expand-sm shadow p-2 mb-5 posinavbar" id="homenav">
      <div class="container-fluid">
        <a class="navbar-brand" href="/">
          <div class="d-flex mt-3 ps-5">
            <i class="fa-solid fa-stopwatch fa-15x text--orange ms-1 py-2"></i>
            <div class="vertical-line ms-2"></div>
            <p class="ps-3">Presto.it</p>
            
          </div>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mb-2 mb-lg-0">
            
            
            
            
            <div class="form-control searchbar ms-5">
              <form action="{{route('searchArticles')}}" method="GET" class="d-flex" >
                <input name="searched" class="form-control-navbar searchbar" type="search" placeholder="{{__('ui.search')}}" aria-label="Search">
                <button class="btn" type="submit" id="button-addon1"><i class="fa-solid fa-magnifying-glass text--white"></i></button>
              </form>
              
            </div>
          </ul>
          
          <ul class="navbar-nav mb-2 mb-lg-0">
            @auth
            
            
            
            
            
            
            <li class="nav-item">
              <a class="nav-link" href="{{route('articleForm')}}">
                <i class="fa-solid fa-circle-plus text--metal fa-15x" alt="add article"></i>
              </a>
            </li>
            
            
            
            
            
            
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-user text--white fa-15x"></i><span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg--orange">{{App\Models\Article::toBeRevisionedCount()}}
                  <span class="visually-hidden">unread messages</span>
                </span>
              </a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="#">{{Auth::user()->name}}</a></li>
                <li><a class="nav-link" href="{{route('myIndex')}}">i miei annunci</a></li>

                <li><hr class="dropdown-divider"></li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">Logout</i></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
                </form>
                <li>
                  <hr class="dropdown-divider">
                </li>
                @if (Auth::user()->is_revisor)
                <li class="nav-item mt-2">
                  <div class="d-flex flex-column">
                    <a href="{{route('indexRevisor')}}" class="nav-link position-relative ps-1" aria-current="page"> {{__('ui.revisor2')}}
                      <span class="badge rounded-pill bg--orange">{{App\Models\Article::toBeRevisionedCount()}}
                        <span class="visually-hidden">unread messages</span>
                        </span>
                    </a>
      
                  </div>
                  
                  
                </li>
                @endif
                
              </ul>
            </li>
            
            
            
            
            
            
            
            @else
            
            
            
            
            
            
            <li class="nav-item dropdown ps-4 mt-1 me-0">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-user-plus ms-2 text--white fa-15x"></i>
              </a>
              <ul class="dropdown-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('login')}}">
                    <p>{{__('ui.login2')}}</p>
                  </a>
                </li>
                <li>
                  <hr class="dropdown-divider">
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('register')}}">
                    <p>{{__('ui.register2')}}</p>
                  </a>
                </li>
              </ul>
            </li>
            @endauth
          </ul>
        </div>
      </div>
      
    </nav>
    
  </div>
  
  
  
  
  
  
  
  
  
  <div class="row">
    <div class="col-12"> 
    </div>
    <div class="swiper mySwiper">
      <div class="swiper-wrapper customheader">
        <div class="swiper-slide">
          <img src="media\mondo.jpeg" />
        </div>
        <div class="swiper-slide customheader">
          <img src="media\keys.jpeg" />
        </div>
        <div class="swiper-slide customheader">
          <img src="media\watch.jpeg" />
        </div>
        <div class="swiper-slide customheader">
          <img src="media\watches.jpeg" />
        </div>
      </div>
    </div>
    
  </div>
  
  
  
  
  
  
  
  <div class="row justify-content-center">
    <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
      <h1 class="headtitle display-1 animate__animated animate__fadeIn posititle gx-0 text-center ">PRESTO.IT</h1>
      <p class="posipayoff gx-0 font-size-payoff text-center animate__animated animate__fadeIn">{{__('ui.payoff')}}</p>
      <a href="{{route('articleForm')}}"><button class="btn-header posi-btn-header">{{__('ui.create-ads')}}</button></a>
    </div>
    
  </div>
</header>