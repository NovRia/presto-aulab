<x-layout>
 <div class="container-fluid vh-100 bgLogin mt-5">
       
        <div class="row justify-content-center mt-5">
             <h1 class="titlespag display-4 px-4 h2 text-center my-5 py-2">{{__('ui.login')}}</h1>
            <div class="col-11 col-xl-6 col-lg-8 col-md-10 bgform">
                <form action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                      <label class="form-label">{{__('ui.email')}}</label>
                      <input type="email" class="form-control" name="email">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">{{__('ui.psw')}}</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <p class="small">{{__('ui.no-reg')}}<a href="{{route('register')}}"> {{__('ui.click')}}</a></p>
                    
                    <p class="d-flex justify-content-center"><button type="submit" class="btn shadow btnbg text--white mt-3 px-5">{{__('ui.login2')}}</button></p>
                  </form>
            </div>
        </div>
    </div>

    <x-footer/>


</x-layout>

