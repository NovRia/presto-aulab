<x-layout>
    
    {{-- titolo articolo + messaggio che indica se ci sono o meno articoli da revisionare --}}
    <div class="container-fluid mt-5 p-5 mb-4 min-vh-100 bgCategory">
        <div class="row pt-3 pb-3">
            <div class="col-12 titlespag p-3 d-flex justify-content-center">
                <h1 class="display-3">
                    {{-- {{$article_to_check ? '{{__('ui.ads-rev1')}}' : '{{__('ui.ads-rev2')}}'}} --}}
                    @if ($article_to_check==null)
                    <div class="display-3">
                        {{__('ui.ads-rev2')}}
                    </div>
                    @elseif ($article_to_check!=null)
                    <div class="display-3">
                        {{__('ui.ads-rev1')}}
                    </div>
                    
                    @endif
                </h1>
            </div>
        </div>
        
        
        {{-- messaggio di conferma --}}
        @if (session('message1'))
        <div class="alert alert-success">
            {{__('ui.message1')}}
        </div>
        
        
        
        @elseif (session('message2'))
        <div class="alert alert-danger">
            {{__('ui.message2')}}
        </div>
        
        @endif
        
        
        
        
        
        
        
        
        {{-- controllo articoli --}}
        @if ($article_to_check)
        
        
        
        
        {{-- card alternativa --}}
        <div class="row mt-5 justify-content-center border-smartphone">
            <div class="col-12 col-md-4 col-lg-3 card-custom-detright display-res-lg">
                <div class="card-body mt-4 d-flex flex-column justify-content-around">
                    <h5 class="card-title h3 mb-3 titles ps-3 op-100">{{$article_to_check->title}}</h5>
                    <p class="card-text h6 ps-3 op-100"> {{$article_to_check->category->type_it}}</p>
                    <p class="card-text h4 ps-3 op-100 pt-3">€ {{$article_to_check->price}}</p>
                    <p class="card-text lead my-4 ps-3 op-100 fst-italic">"{{$article_to_check->description}}"</p>
                    <p class="card-text h6 ps-3 op-100">{{__('ui.date')}}: {{$article_to_check->created_at->format('d/m/y')}}</p>
                    <p class="card-text h6 ps-3 pb-3 op-100">{{__('ui.author')}}: {{$article_to_check->user->name ?? ''}}</p>
                </div>
            </div>
            <div class ="col-12 col-md-8 col-lg-6 card-custom-det p-4 display-res-lg">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    @if (!$article_to_check->images->isEmpty())
                    <div class="carousel-inner">
                        @foreach ($article_to_check->images as $image)
                        <div class="carousel-item @if($loop->first)active @endif">
                            <img src="{{Storage::url($image->path)}}" class="img-fluid p-3 rounded" alt="...">
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://picsum.photos/600" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/602" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/601" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    @endif
                    
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                        <span aria-hidden="true"><i class="fa-solid fa-caret-right fa-3x"></i></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
            {{-- card responsive --}}
            
            
            
            <div class =" col-12 card-custom-det-res d-flex justify-content-center p-0">
                <div id="carouselExampleFade" class="carousel slide carousel-fade display-res" data-bs-ride="carousel">
                    @if (!$article_to_check->images->isEmpty())
                    <div class="carousel-inner">
                        @foreach ($article_to_check->images as $image)
                        <div class="carousel-item @if($loop->first)active @endif">
                            <img src="{{Storage::url($image->path)}}" class="img-fluid rounded" alt="...">
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://picsum.photos/600" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/602" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/601" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    @endif
                    
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                        <span aria-hidden="true"><i class="fa-solid fa-caret-right fa-3x"></i></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-12 card-custom-detright-res display-res">
                <div class="card-body mt-4 d-flex flex-column justify-content-around">
                    <h5 class="card-title h3 mb-3 titles ps-3 op-100">{{$article_to_check->title}}</h5>
                    <p class="card-text h6 ps-3 op-100"> {{$article_to_check->category->type_it}}</p>
                    <p class="card-text h4 ps-3 op-100 pt-3">€ {{$article_to_check->price}}</p>
                    <p class="card-text lead my-4 ps-3 op-100 fst-italic">"{{$article_to_check->description}}"</p>
                    <p class="card-text h6 ps-3 op-100">{{__('ui.date')}}: {{$article_to_check->created_at->format('d/m/y')}}</p>
                    <p class="card-text h6 ps-3 pb-3 op-100">{{__('ui.author')}}: {{$article_to_check->user->name ?? ''}}</p>
                </div>
            </div>
            
            
            
            
        </div>
        
        {{-- bottoni per accettare/rifiutare l'articolo --}}
        <div class="row mt-3 justify-content-center">
            <div class="col-6 col-lg-4 d-flex justify-content-center">
                <form action="{{route('revisorAcceptArticle', ['article'=>$article_to_check])}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class="btnbgRevisor shadow-green text--green">{{__('ui.accept')}}</button>
                </form>
            </div>
            <div class="col-6 col-lg-4 d-flex justify-content-center">
                <form action="{{route('revisorRejectArticle', ['article'=>$article_to_check])}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div>
                        <button type="submit" class="btnbgRevisor shadow-red text-danger">{{__('ui.reject')}}</button>
                    </div>
                </form>
                
                
            </div>
        </div>
    </div>
    
    @endif
</div>
<x-footer></x-footer>
</x-layout>