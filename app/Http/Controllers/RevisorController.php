<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Mail\RequestRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function indexRevisor(){
        $article_to_check = Article::where('is_accepted', null)->first();
        return view('indexRevisor', compact('article_to_check'));
    }

    public function acceptArticle (Article $article){
        $article->setAccepted(true);
        return redirect()->back()->with('message1', 'Complimenti, hai accettato l\'articolo');
    }

    public function rejectArticle (Article $article){
        $article->setAccepted(false);
        return redirect()->back()->with('message2', 'Complimenti, hai rifiutato l\'articolo');
    }

    public function requestRevisor(){
        Mail::to('admin@presto.it')->send(new RequestRevisor(Auth::user()));
        return redirect('/')->with('message3', 'Complimenti! Hai richiesto di diventare revisore correttamente!');
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor', ["email"=>$user->email]);
        return redirect('/')->with('message4', 'Complimenti! L\'utente è diventato revisore');
    }


}
