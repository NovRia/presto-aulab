<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function articleForm(){
        return view ('articleForm');
    }
    
    public function welcome(){
        /* importante che l'orderBy sia dopo where perchè deve ordinarlo subito */
        $articles = Article::where('is_accepted', true)->orderBy('created_at', 'DESC')->take(8)->get();
        return view ('welcome', compact('articles'));
    }
    /* public function welcome2(){
        $categories = Category::all();
        return view ('welcome', compact('categories'));
    } */
    
    /* chiedere a roberto come inserire is_accepted in questa funzione */
    public function showCategory(Category $category){
        return view ('showCategory', compact('category'));
    }
    // pagina del dettaglio dell'articolo
    public function showArticle(Article $article){
        
        return view('showArticle', compact('article'));
    }
    
    
    // funzione che mostra tutti gli annunci
    public function indexArticle(){
        $articles = Article::where('is_accepted',true)->orderBy('created_at', 'DESC')->paginate(8);
        return view ('indexArticle', compact('articles'));
    }
    
    // funzione che mostra tutti gli annunci dell'utente loggato
    public function myIndex(){
        // dd($num);
        $nums = [ 1,2,3];
        $articles = Article::where('user_id',Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(6);
        
        return view ('myIndex', compact('articles', 'nums'));
    }
    
    // funzione che mostra tutti gli annunci di uno specifico utente
    public function authorIndex(Article $article){
        $articles = Article::where('user_id', $article->user->id)->orderBy('created_at', 'DESC')->paginate(8);
        
        return view('authorIndex', compact('articles', 'article'));
    }
    
    
    
    //funzione per la ricerca di un articolo specifico 
    public function searchArticles(Request $request){
        $articles = Article::search($request->searched)->where('is_accepted', true);
        return view('indexArticle', compact('articles'));
    }
    
    
    public function setLanguage($lang){
        /* dd($lang); */
        session()->put('locale', $lang);
        return redirect()->back();
    }
}
